from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import json
import pandas as pd
import time
import os

# Setting path
url = 'https://app.clickup.com/'
landing_page_url = 'https://app.clickup.com/4589271/v/b/6-7744681-2'
case_url = "https://app.clickup.com/t/4589271/"

settings = {
    "recentDestinations": [{
        "id": "Save as PDF",
        "origin": "local",
        "account": ""
    }],
    "selectedDestinationId": "Save as PDF",
    "version": 2,
    "isHeaderFooterEnabled": False,
    # "mediaSize": {
    #     "height_microns": 210000,
    #     "name": "ISO_A5",
    #     "width_microns": 148000,
    #     "custom_display_name": "A5"
    # },
    "customMargins": {},
    # "marginsType": 2,
    # "scaling": 175,
    # "scalingType": 3,
    # "scalingTypePdf": 3,
    "isCssBackgroundEnabled": True
}

prefs = {
    'printing.print_preview_sticky_settings.appState': json.dumps(settings),
    'savefile.default_directory': '/home/easebuzz/Downloads/case_pdfs'
}

def launchBrowser():
    driver_path = "/home/easebuzz/Downloads/chromedriver"
    email = "intern@easebuzz.in"
    password = "Prasad@1992"

    # Open Website
    chrome_options = Options()
    chrome_options.binary_location="/usr/bin/google-chrome"
    # chrome_options.add_argument("--headless")
    chrome_options.add_argument("start-maximized")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument("--kiosk-printing")
    chrome_options.add_experimental_option('prefs', prefs)
    driver = webdriver.Chrome(driver_path, options=chrome_options)
    driver.get(landing_page_url)

    # Validate ad Login
    email_element = driver.find_element_by_id("login-email-input")
    pass_element = driver.find_element_by_id("login-password-input")
    submit_button_element = driver.find_element_by_class_name("login-page-new__main-form-button")
    email_element.send_keys(email)
    pass_element.send_keys(password)
    submit_button_element.click()

    # Reading file
    columns = ["S No", "MID", "Case ID", "Stage", "Bucket", "Assigned personnel", "Reporting", "Export completed (Yes/ No)"]
    df = pd.read_excel('/home/easebuzz/script/Clickup Data Migration Status.xlsx', usecols=columns)
    # cases = df['Case ID'].to_list()
    people = df["Assigned personnel"].unique().tolist()
    # print(people)
    person = df.loc[df['Assigned personnel'] == people[1]]
    # print(person)
    cases = person['Case ID'].to_list()
    mids = person['MID'].to_list()

    for case in cases:
        modified_url = case_url + case
        time.sleep(6)
        driver.get(modified_url)
        driver.execute_script("window.print()")
    
launchBrowser()


# people = ['A.Dinesh', 'Aditya Kumar', 'Ajinkya Bule', 'Atishraj Desai', 'Avinash Bhaskar Jha', 'Hritul Pardhi', 'Khushi Gupta', 'Mikha George', 'Nishank Majumdar', 'Purva Mankar', 'Samarth Srivastava', 'Shivendra Singh', 'Somedip Ghosh', 'Tanvi Gurav', 'Tripti Kothari', 'Vasu Samnotra']